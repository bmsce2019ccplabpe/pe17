#include<stdio.h>
#include<math.h>
struct points
{
	float x,y;
};
struct points input()
{
	struct points p;
	printf("Enter points\n");
	scanf("%f %f",&p.x,&p.y);
	return p;
}
float compute(struct points p1,struct points p2)
{
	float distance;
	distance= sqrt(((p2.x-p1.x)*(p2.x-p1.x))+((p2.y-p1.y)*(p2.y-p1.y)));
	return distance;
}
void output(float distance)
{
	printf("The Sum of two numbers is %f\n",distance );
	
}
float main()
{
	float distance;
	struct points p1,p2;
	p1=input();
	p2=input();
	distance=compute(p1,p2);
	output(distance);
	return 0;
}