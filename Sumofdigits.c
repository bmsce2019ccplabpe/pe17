#include<stdio.h>
int input()
{
    int a;
    printf("Enter a number\n");
    scanf("%d",&a);
    return a;
}

int compute(int a)
{
    int sum=0,r;
    while(a>0)
    {
        r=a%10;
        sum = sum + r;
        a=a/10;
    }
    return sum;
}

void output(int a)
{
    printf("The sum of digits is %d\n",a);
}

int main()
{
    int a,sum;
    a=input();
    sum=compute(a);
    output(sum);
    return 0;
}