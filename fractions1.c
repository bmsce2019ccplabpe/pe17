#include <stdio.h>  
struct fractions
{
	int n,d;
};
struct fractions input()
{
	struct fractions f;
	printf("Enter numerator and denominator for fractions\n");
	scanf("%d %d",&f.n,&f.d);
	return f;
}
struct fractions compute(int n1,int d1,int n2,int d2)
{
	struct fractions f;
	
	f.n=(n1*d2)+(n2*d1);
	f.d=d1*d2;
	return f;
}
void output(struct fractions f)
{
	printf("The Sum of Fractions is %d/%d\n",f.n,f.d);
}
int gcd(int a,int b)
{
	int t;
	while(b!=0)
	{
		t =b;
		b= a%b;
		a=t;
	}
    return t;
}
void simplify(int a,int b)
{
	int n,d;
	n=a/gcd(a,b);
	d=b/gcd(a,b);
	printf("Simplifying %d/%d we get : %d/%d\n",a,b,n,d);
}
int main()
{
	struct fractions f1,f2,f;
	f1=input();
	f2=input();
	f=compute(f1.n,f1.d,f2.n,f2.d);
	output(f);
	simplify(f.n,f.d);
	return 0;
}