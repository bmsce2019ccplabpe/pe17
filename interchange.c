#include<stdio.h>
void read_array (int a[], int n)
{
  int i;
  printf ("Enter elements of the array \n");
  for (i = 0; i < n; i++)
    {
      scanf ("%d", &a[i]);
    }
}


int find_smallest (int a[], int n)
{
  int i, small = a[0], small_pos = 0;
  for (i = 0; i < n; i++)
    {
      if (a[i] < small)
	{
	  small = a[i];
	  small_pos = i;
	}
    }
  printf ("The smallest number in the array is %d at position %d \n", small,
	  small_pos);
  return small_pos;
}


int find_largest (int a[], int n)
{
  int i, large = a[0], large_pos = 0;
  for (i = 0; i < n; i++)
    {
      if (a[i] > large)
	{
	  large = a[i];
	  large_pos = i;
	}
    }
  printf ("The largest number in the array is %d at position %d \n", large,
	  large_pos);
  return large_pos;
}


void interchange (int a[], int large_pos, int small_pos, int n)
{
  int temp;
  temp = a[large_pos];
  a[large_pos] = a[small_pos];
  a[small_pos] = temp;
}


void print_array (int a[], int n)
{
  int i;
  printf ("The final array after interchange is \n");
  for (i = 0; i < n; i++)
    {
      printf ("%d \t", a[i]);
    }
	printf("\n");
}

int main ()
{
  int a[20], i, n, small_pos, large_pos;
  printf ("Enter the size of the array \n");
  scanf ("%d", &n);
  read_array (a, n);
  small_pos = find_smallest (a, n);
  large_pos = find_largest (a, n);
  interchange (a, large_pos, small_pos, n);
  print_array (a, n);
  return 0;
}