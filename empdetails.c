#include<stdio.h>
struct employee
{
    int age,salary;
    char name[25],id[20];
}emp[100];
 
void main()
{
    int i,n;
    printf("Enter the no of employees\n");
    scanf("%d",&n);
    
    for(i=0;i<n;i++)
    {
		printf("Enter %d employee id , name , age and salary\n",i+1);
        scanf("%s %s %d %d",emp[i].id,emp[i].name,&emp[i].age,&emp[i].salary);
    }
    printf("\nEMP_ID\tEMP_NAME\tEMP_AGE\t\tEMP_SAL\n");
    for(i=0;i<n;i++)
    {
        printf("%s\t\t%s\t\t%d\t\t%d\n",emp[i].id,emp[i].name,emp[i].age,emp[i].salary);
    }
}