#include<stdio.h>
void swap(int *a,int*b)
{
    int temp;
    temp=*a;
    *a=*b;
    *b=temp;
}
    

int main()
{
    int a,b;
    printf("Enter the 2 numbers\n");
    scanf("%d %d",&a,&b);
    swap(&a,&b);
    printf("Numbers after swapping are: %d %d",a,b);
    return 0;
}
 
