#include<stdio.h>
struct power
{
    int a,b;
};    
struct power input()
{
    struct power p;
    printf("enter the number and the power to which it is to be raised\n");
    scanf("%d %d",&p.a,&p.b);
    return p;
}

int compute(struct power p)
{
    int i,pow=1;  
    if(p.b==0)
        return 1;    
    else 
    {
        for(i=0;i<p.b;i++)
        {
            pow=pow*p.a;
        }
        return pow;
    }
}        
void output(int a)
{
    printf("the power of number is %d\n",a);   
}

int main()
{
    int pow;
    struct power x;
    x=input();
    pow=compute(x);
    output(pow);
    return 0;
}    