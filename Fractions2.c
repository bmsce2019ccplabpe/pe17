#include <stdio.h>
struct fractions
{
	int a[100],b[100],n,d;
};

int gcd(int a,int b)
{
	int t;
	while(b!=0)
	{
		t =b;
		b= a%b;
		a=t;
	}
    return t;
}

struct fractions input(int l)
{
	struct fractions f;
	int i;
	for(i=0;i<l;i++)
	{
		printf("Enter  fraction number %d as a/b\n",i+1);
		scanf("%d/%d",&f.a[i],&f.b[i]);
	}
	return f;
}
struct fractions compute(struct fractions f,int l)
{
	int i,lcm;
	f.n=0;
	f.d=0;
	for(i=0;i<l;i++)
	{
		lcm = (f.d>f.b[i]) ? f.d : f.b[i];
		while(1)
		{
			if (f.d==0)
			{
				lcm=f.b[i];
				break;
			}
			else if( lcm%f.d==0 && f.d%f.b[i]==0 )
				break;
			lcm++;
		}
		if(f.d==0)
		{
			f.n=((f.a[i])*(lcm)/(f.b[i]));
		}
		else if(f.d!=0)
		{
			f.n= ((f.n)*(lcm)/(f.d)) + ((f.a[i])*(lcm)/(f.b[i]));
		}
		f.d= lcm;
	}
	f.n=f.n/gcd(f.n,f.d);
	f.d=f.d/gcd(f.n,f.d);
	return f;
}
void output(struct fractions f)
{
	printf("%d/%d",f.n,f.d);
}
int main()
{
	int l;
	struct fractions f,r;
	printf("Enter the number of fractions\n");
	scanf("%d",&l);
	f=input(l);
	r=compute(f,l);
	output(r);
	system("pause");
	return 0;
}