#include <stdio.h>
struct fractions
{
	int n,d;
};

int gcd(int a,int b)
{
	int t;
	while(b!=0)
	{
		t =b;
		b= a%b;
		a=t;
	}
    return t;
}
void input(struct fractions f[],int l)
{
	int i;
	for(i=0;i<l;i++)
	{
		printf("Enter  fraction number %d as a/b\n",i+1);
		scanf("%d/%d",&f[i].n,&f[i].d);
	}
}
struct fractions compute(struct fractions f[],int l)
{
	int i;
	struct fractions g;
	if(l==1)
	{
		g.n=f[0].n;
		g.d=f[0].d;
	}
	else 
	{
		g.n=((f[0].n)*(f[1].d))+((f[1].n)*(f[0].d));
		g.d=(f[0].d)*(f[1].d);
		for(i=2;i<l;i++)
		{
			g.n=((g.n)*(f[i].d))+((f[i].n)*(g.d));
			g.d=(g.d)*(f[i].d);
		}
	}
	return g;
}
void output(struct fractions g)
{
	int a,b;
	a=g.n/gcd(g.n,g.d);
	b=g.d/gcd(g.n,g.d);
	printf("Sum of Fractions is : %d/%d\n",a,b);
}

int main()
{
	int l;
	struct fractions f[100],r;
	printf("Enter the number of fractions\n");
	scanf("%d",&l);
	input(f,l);
	r=compute(f,l);
	output(r);
	return 0;
}
