#include<stdio.h>
int main()
{
    int n,pos,ele,c;
    printf("enter the number of elements in array\n");
    scanf("%d",&n);
    int a[n+1],i=0;
    printf("enter elements into array\n");
    for(i=0;i<n;i++)
        scanf("%d",&a[i]);
    printf("Enter 0 if you want to insert element into array or 1 if you want to delete element from array\n"); 
    scanf("%d",&c);   
    if(c==0)   
    {
        printf("enter the number and the position in which element should be inserted in array\n");
        scanf("%d %d",&ele,&pos);
        for (i=n-1;i>=pos;i--)
        {
            a[i+1]=a[i];
        }
        a[pos]=ele;
        printf("Array after insertion is\n");
        for(i=0;i<=n;i++)
            printf("%d\t",a[i]);
    }
    else if(c==1)
    {
        printf("enter position of element which should be deleted from array\n");
        scanf("%d",&pos);
        for(i=pos;i<n-1;i++)
            a[i]=a[i+1];
        printf("array after deletion is\n");
        for(i=0;i<n-1;i++)
            printf("%d\t",a[i]);
    }
    return 0;            
}      